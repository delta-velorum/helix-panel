/*
* Created by Michel3951
* Date: 6/14/2020 at 7:05 PM
* Discord: Michel3951#6705
*/

import * as types from '../mutations';
import axios from "../../plugins/axios";

export const state = {
    user: null,
};

// getters
export const getters = {
    user: state => state.user,
};

// mutations
export const mutations = {
    [types.FETCH_USER_SUCCESS](state, data) {
        state.user = data.data;
    },

    [types.FETCH_USER_FAIL](state) {
        state.filters = null;
    },
};

// actions
export const actions = {
    async fetch({commit}) {
        try {
            let {data} = await axios.get('/api/users/me');


            commit(types.FETCH_USER_SUCCESS, data);
        } catch (e) {
            commit(types.FETCH_USER_FAIL);
        }
    },
};
