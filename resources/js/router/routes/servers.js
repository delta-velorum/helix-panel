/*
* Created by Michel3951
* Date: 6/14/2020 at 6:56 PM
* Discord: Michel3951#6705
*/

import Index from "../../components/views/servers/Index";
import Create from "../../components/views/servers/Create";

import Authenticated from "../middleware/auth";

export default [
    {
        path: '/servers',
        component: Index,
        name: 'servers',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Servers',
                }
            ]
        }
    },
    {
        path: '/servers/create',
        component: Create,
        name: 'servers/create',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Servers',
                    to: {name: 'servers'}
                },
                {
                    text: 'Create',
                }
            ]
        },
    },
];