/*
* Created by Michel3951
* Date: 6/14/2020 at 6:56 PM
* Discord: Michel3951#6705
*/

import Index from "../../components/views/management/users/Index";
import Create from "../../components/views/management/users/Create";
import Edit from "../../components/views/management/users/Edit";
import Activity from "../../components/views/management/users/Activity";

import Authenticated from "../middleware/auth";

export default [
    {
        path: '/users',
        component: Index,
        name: 'users',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Users',
                }
            ]
        }
    },
    {
        path: '/users/create',
        component: Create,
        name: 'users/create',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Users',
                    to: {name: 'users'}
                },
                {
                    text: 'Create',
                }
            ]
        },
    },
    {
        path: '/users/:id',
        component: Edit,
        name: 'users/edit',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Users',
                    to: {name: 'users'}
                },
                {
                    text: 'Modify',
                }
            ]
        }
    },
    {
        path: '/users/:id/activity',
        component: Activity,
        name: 'users/activity',
        meta: {
            middleware: [
                Authenticated
            ],
            breadcrumbs: [
                {
                    text: 'Users',
                    to: {name: 'users'}
                },
                {
                    text: 'Activity',
                }
            ]
        }
    },
];