/*
* Created by Michel3951
* Date: 6/14/2020 at 6:56 PM
* Discord: Michel3951#6705
*/

import Login from "../../components/views/auth/Login"
import Guest from "../middleware/guest";

export default [
    {
        path: '/login',
        component: Login,
        name: 'login',
        meta: {
            middleware: [
                Guest
            ]
        }
    },
];