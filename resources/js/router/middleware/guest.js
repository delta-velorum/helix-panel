export default async function Guest({next, store}) {

    await store.dispatch('auth/fetch');

    if (store.getters['auth/user']) {
        return next({
            name: 'home'
        });
    }

    return next()
}
