export default async function Authenticated({next, store}) {

    await store.dispatch('auth/fetch');

    if (!store.getters['auth/user']) {
        return next({
            name: 'login'
        });
    }

    return next()
}
