/*
* Created by Michel3951
* Date: 6/14/2020 at 6:51 PM
* Discord: Michel3951#6705
*/

import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";

Vue.use(VueRouter);

// Routes
import auth from "./routes/auth";
import users from "./routes/users";
import servers from "./routes/servers";

// Components
import Home from "../components/views/Home";
import Setup from "../components/views/Setup";

// Middleware
import Authenticated from "./middleware/auth";

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/panel',
            component: Home,
            name: 'home',
            meta: {
                middleware: [
                    Authenticated
                ]
            }
        },

        {
            path: '/setup',
            component: Setup,
            name: 'setup',
        },

        ...auth,
        ...users,
        ...servers
    ]
});

router.beforeEach((to, from, next) => {
    if (!to.meta.hasOwnProperty('middleware')) {
        return next()
    }

    const middleware = to.meta.middleware;

    const context = {
        to,
        from,
        next,
        store
    };

    return middleware[0]({
        ...context,
    })
});

export default router;