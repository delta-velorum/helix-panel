/*
* Created by Michel3951
* Date: 6/14/2020 at 6:21 PM
* Discord: Michel3951#6705
*/

import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import { ValidationObserver, ValidationProvider, extend } from 'vee-validate';
import { messages } from 'vee-validate/dist/locale/en.json';

import * as rules from 'vee-validate/dist/rules';
import router from "./router";
import store from "./store";
import Layout from "./components/Layout";
import Breadcrumbs from "./components/Breadcrumbs";

Vue.use(BootstrapVue);

Object.keys(rules).forEach(rule => {
    extend(rule, {
        ...rules[rule], // copies rule configuration
        message: messages[rule] // assign message
    });
});

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('Breadcrumbs', Breadcrumbs);

const app = new Vue({
    template: `<layout></layout>`,
    components: { Layout },
    render: h => h(Layout),
    router,
    store
}).$mount('#app');