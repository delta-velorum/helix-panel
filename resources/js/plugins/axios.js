/*
* Created by Michel3951
* Date: 6/14/2020 at 7:07 PM
* Discord: Michel3951#6705
*/

import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.withCredentials = true;

export default axios;