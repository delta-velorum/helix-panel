<?php
/*
* Created by Michel3951
* Date: 6/20/2020 at 7:58 PM
* Discord: Michel3951#6705
*/


namespace App\Services\Server;


use App\Models\Server;
use App\Support\Nitrado\Resources\Services\Service;

class UpdateService
{
        public $server;

        public function __construct($id)
        {
            $server = Server::find($id);

            $this->server = $server;
        }

        public function fromService(Service $service)
        {
            $this->server->update([
                'has_auto_extension' => $service->autoExtension,
                'auto_extension_duration' => $service->autoExtensionDuration,
                'slots' => $service->details->slots,
                'slots' => $service->details->slots,
            ]);
        }

        public function fromServer()
        {

        }
}