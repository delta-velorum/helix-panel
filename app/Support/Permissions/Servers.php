<?php
/*
* Created by Michel3951
* Date: 6/15/2020 at 9:38 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Permissions;


class Servers
{
    const ADD_SERVERS = 'add servers';
    const MODIFY_SERVERS = 'modify servers';
    const DELETE_SERVERS = 'delete servers';
    const LIST_SERVERS = 'list servers';
}