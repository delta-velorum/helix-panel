<?php
/*
* Created by Michel3951
* Date: 6/15/2020 at 9:38 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Permissions;


class Users
{
    const MODIFY_USER = 'modify user';
    const CREATE_USER = 'create user';
    const DELETE_USER = 'delete user';
    const LIST_USERS = 'list users';
}