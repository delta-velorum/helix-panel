<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:37 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado;


use App\Support\Nitrado\Endpoints\GameServers\GameServersEndpoint;
use App\Support\Nitrado\Endpoints\Services\ServicesEndpoint;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;

class NitradoApiClient
{
    /**
     * Client version
     */
    const CLIENT_VERSION = "1.0.0";

    /**
     * API Endpoint
     */
    const ENDPOINT = "https://api.nitrado.net/";

    /**
     * Request timeout
     */
    const TIMEOUT = 10;

    /**
     * RESTful Services endpoint
     *
     * @var ServicesEndpoint $services
     */
    public $services;

    /**e
     * RESTful GameServers endpoint
     * @var GameServersEndpoint $gameservers
     */
    public $gameServers;

    public $httpClient;

    public $apiKey;

    public function __construct(ClientInterface $httpClient = null)
    {
        $this->httpClient = $httpClient? $httpClient : new Client([
            RequestOptions::VERIFY => false,
            RequestOptions::TIMEOUT => self::TIMEOUT,
        ]);

        $this->initialize();
    }

    protected function initialize()
    {
        $this->services = new ServicesEndpoint($this);
        $this->gameServers = new GameServersEndpoint($this);
    }

    public function setApiKey($key)
    {
        $this->apiKey = $key;
        return $this;
    }
}