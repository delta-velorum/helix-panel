<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:48 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Resources\Services;

use App\Support\Nitrado\Resources\BaseResource;

class Service extends BaseResource
{
    /**
     * The unique ID of this service
     * @var $id
     */
    public $id;

    /**
     * The location ID of this service
     * @var $location_id
     */
    public $location_id;

    /**
     * The status of this service
     * @var $status
     */
    public $status;

    /**
     * ???
     * @var $websocket_token
     */
    public $websocket_token;

    /**
     * The service's username
     * @var $username
     */
    public $username;

    /**
     * Is auto-renewal enabled
     * @var $auto_extension
     */
    public $auto_extension;

    /**
     * The date this service will be renewed, if enabled
     * @var $auto_extension_duration
     */
    public $auto_extension_duration;

    /**
     * The type of this service
     * @var $type
     */
    public $type;

    /**
     * The package name of this service
     * @var $type_human
     */
    public $type_human;

    /**
     * The date this service was created
     * @var $start_date
     */
    public $start_date;

    /**
     * The date this service will be suspended, if not renewed
     * @var $suspend_date
     */
    public $suspend_date;

    /**
     * The date this service will be deleted after suspension
     * @var $delete_date
     */
    public $delete_date;

    /**
     * Servername, slots etc
     * @var $details
     */
    public $details;

    public function gameServer()
    {

    }
}