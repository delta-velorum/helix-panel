<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:55 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Resources;

class ResourceFactory
{
    /**
     * Create resource object from Api result
     *
     * @param \stdClass $result
     * @param BaseResource $resource
     *
     * @return BaseResource
     */
    public static function createFromApiResult($result, BaseResource $resource)
    {
        if (property_exists($result, 'data')) {
            $result = $result->data;
        }

        foreach ($result as $property => $value) {
            $resource->{$property} = $value;
        }

        return $resource;
    }
}