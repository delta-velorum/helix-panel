<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:48 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Resources\GameServers;

use App\Support\Nitrado\Resources\BaseResource;

class GameServer extends BaseResource
{
    /**
     * The unique ID of this service
     * @var $id
     */
    public $service_id;

    /**
     * The owner's ID
     * @var $location_id
     */
    public $user_id;

    /**
     * The location id of this server
     * @var $location_id
     */
    public $location_id;

    /**
     * The status of this service
     * @var $status
     */
    public $status;

    /**
     * ???
     * @var $websocket_token
     */
    public $websocket_token;

    /**
     * The system this server is running on
     * @var $hostsystems
     */
    public $hostsystems;

    /**
     * The service's username
     * @var $username
     */
    public $username;



    public function gameServer()
    {

    }
}