<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:56 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Resources;


abstract class BaseResource
{
    public function toJson()
    {
        return json_encode($this);
    }
}