<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:56 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Endpoints;

use App\Support\Nitrado\NitradoApiClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

abstract class BaseEndpoint
{
    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';
    const HTTP_UPDATE = 'PATCH';
    const HTTP_DELETE = 'DELETE';

    /**
     * @var NitradoApiClient
     */
    protected $client;

    protected $path;

    /**
     * @param $client
     */
    public function __construct(NitradoApiClient $client)
    {
        $this->client = $client;
    }

    public function httpCall($method)
    {
        $guzzle = $this->client->httpClient;

        try {
            $request = $guzzle->request($method, $this->client::ENDPOINT . $this->path, [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->client->apiKey
                ]
            ]);
            $response = $request->getBody()->getContents();
            return json_decode($response);
        } catch (RequestException $exception) {
            abort($exception->getCode());
        }
        return null;
    }
}