<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:45 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Endpoints\Services;


use App\Support\Nitrado\Endpoints\BaseEndpoint;
use App\Support\Nitrado\Resources\ResourceFactory;
use App\Support\Nitrado\Resources\Services\Service;

class ServicesEndpoint extends BaseEndpoint {
    protected $path = "services";

    public function all()
    {
        $response = $this->httpCall('GET');

        return array_map(function ($service) {
            return ResourceFactory::createFromApiResult($service, new Service());
        }, $response->data->services);
    }

    public function get($id)
    {
        $this->path = $this->path . '/' . $id;

        try {
            $response = $this->httpCall('GET');
        } catch (\Exception $exception) {
            $response = null;
        }

        return $response ? new Service($response) : $response;
    }
}