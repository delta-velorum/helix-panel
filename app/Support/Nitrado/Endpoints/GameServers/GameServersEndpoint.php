<?php
/*
* Created by Michel3951
* Date: 6/16/2020 at 8:45 PM
* Discord: Michel3951#6705
*/


namespace App\Support\Nitrado\Endpoints\GameServers;


use App\Support\Nitrado\Endpoints\BaseEndpoint;
use App\Support\Nitrado\Resources\GameServers\GameServer;
use App\Support\Nitrado\Resources\ResourceFactory;
use App\Support\Nitrado\Resources\Services\Service;
use Illuminate\Support\Collection;

class GameServersEndpoint extends BaseEndpoint
{
    protected $path = "services/%d/gameservers";

    public function all()
    {
        $response = $this->httpCall('GET');

        return array_map(function ($service) {
            return ResourceFactory::createFromApiResult($service, new Service());
        }, $response->data->services);
    }

    public function get($id)
    {
        $this->path = sprintf($this->path, $id);

        try {
            $response = $this->httpCall('GET');
        } catch (\Exception $exception) {
            return null;
        }

        return ResourceFactory::createFromApiResult($response, new GameServer());
    }
}