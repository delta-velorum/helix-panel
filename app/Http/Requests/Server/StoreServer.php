<?php

namespace App\Http\Requests\Server;

use Illuminate\Foundation\Http\FormRequest;

class StoreServer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'api_key' => [
                'required',
                'string'
            ],
            'service_id' => [
                'required',
                'integer'
            ],
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'is_tracking_players' => [
                'boolean',
                'present'
            ],
            'has_status_notifications' => [
                'boolean',
                'present'
            ],
            'status_change_webhook' => [
                'nullable',
                'required_if:has_status_notifications,true',
                'string',
                'regex:/^https:\/\/discordapp\.com\/api\/webhooks\/[0-9]+\/.*$/'
            ]
        ];
    }
}
