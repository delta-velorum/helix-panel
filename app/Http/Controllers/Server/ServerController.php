<?php
/*
* Created by Michel3951
* Date: 6/18/2020 at 9:53 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\Server;

use App\Http\Controllers\Controller;
use App\Http\Requests\Server\StoreServer;
use App\Http\Resources\Server\ServerCollection;
use App\Models\Server;
use App\Support\Nitrado\NitradoApiClient;

class ServerController extends Controller
{
    public function store(StoreServer $request)
    {
        $data = $request->validated();

        $nitrado = new NitradoApiClient();
        $nitrado->setApiKey($data['api_key']);

        $service = $nitrado->gameServers->get($data['service_id']);

        if (!$service) {
            return response()
                ->json([
                    'message' => 'Service not found, make sure the service exists and is running ARK: Survival Evolved.',
                ], 400);
        }

        $data['data'] = $service->toJson();

        $server = Server::create($data);

        return response()
            ->json([
                'data' => $server,
                'message' => $data['name'] . ' was successfully added to the server list.'
            ], 201);
    }

    public function index()
    {
        return new ServerCollection(Server::all());
    }
}