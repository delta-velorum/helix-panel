<?php
/*
* Created by Michel3951
* Date: 6/14/2020 at 7:47 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreUser;
use App\Http\Requests\User\UpdateUser;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    public function me()
    {
        return new UserResource(Auth::user());
    }

    public function index()
    {
        $users = QueryBuilder::for(User::class)
            ->allowedFilters(['name', 'email'])
            ->get();

        return new UserCollection($users);
    }

    public function show(User $user)
    {
        $user->load(['permissions']);

        return new UserResource($user);
    }

    public function activity(User $user)
    {
        $user->load(['permissions', 'activity']);

        return new UserResource($user);
    }

    public function store(StoreUser $request)
    {
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);

        $user = User::create($data);

        $user->givePermissionTo($data['permissions'] ?? []);

        $user->load('permissions');

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log('Created user');

        return new UserResource($user);
    }

    public function update(User $user, UpdateUser $request)
    {
        $data = $request->validated();

        if (array_key_exists('password', $data) && !empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data['password'] = $user->password;
        }

        $user->update($data);

        $user->syncPermissions($data['permissions'] ?? []);

        $user->load('permissions');

        activity()
            ->performedOn($user)
            ->causedBy(Auth::user())
            ->log('Modified user');

        return new UserResource($user);
    }
}