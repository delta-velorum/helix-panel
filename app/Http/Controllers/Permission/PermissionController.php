<?php
/*
* Created by Michel3951
* Date: 6/15/2020 at 9:41 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use App\Http\Resources\Permission\PermissionCollection;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function __invoke()
    {
        return new PermissionCollection(Permission::all(['id','name']));
    }
}