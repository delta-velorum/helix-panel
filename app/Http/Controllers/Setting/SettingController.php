<?php
/*
* Created by Michel3951
* Date: 6/17/2020 at 7:14 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\Setting;


use App\Http\Controllers\Controller;
use App\Http\Resources\Setting\SettingResource;
use App\Models\Setting;

class SettingController extends Controller
{
    public function show(Setting $setting)
    {
        return new SettingResource($setting);
    }
}