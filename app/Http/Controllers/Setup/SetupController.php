<?php
/*
* Created by Michel3951
* Date: 6/14/2020 at 9:36 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\Setup;


use App\Http\Controllers\Controller;
use App\Http\Requests\Setup\StoreSetup;
use App\Http\Resources\UserResource;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SetupController extends Controller
{
    public function __invoke(StoreSetup $request)
    {
        $data = $request->validated();

        $data['password'] = Hash::make($data['password']);
        $data['is_super_admin'] = true;

        $user = User::create($data);

        Setting::create([
            'key' => 'api_key',
            'value' => $data['api_key']
        ]);

        return new UserResource($user);
    }
}