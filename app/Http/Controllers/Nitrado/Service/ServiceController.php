<?php
/*
* Created by Michel3951
* Date: 6/17/2020 at 7:40 PM
* Discord: Michel3951#6705
*/


namespace App\Http\Controllers\Nitrado\Service;


use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Policies\ServerPolicy;
use App\Support\Nitrado\NitradoApiClient;
class ServiceController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = new NitradoApiClient();
        $this->client->setApiKey(
            request()->headers->get('X-Nitrado-Key', Setting::get('api_key'))
        );
    }

    /**
     * List all services that belong to this API key
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', ServerPolicy::class);

        return response()
            ->json([
                'data' => $this->client->services->all()
            ]);
    }

    public function show($id)
    {
        $this->client->services->get($id);
    }
}