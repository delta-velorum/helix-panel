<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'global_settings';

    protected $fillable = [
        'key',
        'value',
        'protected',
    ];

    static function get($key, $default = null)
    {
        return Setting::where('key', $key)->first()->value ?? $default;
    }
}
