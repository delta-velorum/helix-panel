<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $table = 'servers';

    protected $fillable = [
        'name',
        'api_key',
        'status_change_webhook',
        'is_tracking_players',
        'has_status_notifications',
        'data',
    ];

    public function cache()
    {
        return $this->hasMany(ServerCache::class);
    }

    public $casts = [
        'is_tracking_players' => 'boolean',
        'has_status_notifications' => 'boolean',
        'has_auto_extension' => 'boolean',
    ];
}
