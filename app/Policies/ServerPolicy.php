<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * If the user is a super admin we don't need to check permissions
     *
     * @return bool|void
     */
    public function before()
    {
        if (auth()->user()->is_super_admin) {
            return true;
        }
    }

    public function index()
    {
        return auth()->user()->can('add servers');
    }
}
