<?php

use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\Nitrado\Service\ServiceController;
use App\Http\Controllers\Server\ServerController;
use Illuminate\Support\Facades\Route;

Route::post('/setup', 'Setup\SetupController');

Route::group([
    'middleware' => ['auth:sanctum'],
    'prefix' => '/users'
], function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('/', [UserController::class, 'store']);
    Route::get('/me', [UserController::class, 'me']);
    Route::get('/{user:id}/activity', [UserController::class, 'activity']);
    Route::get('/{user:id}', [UserController::class, 'show']);
    Route::post('/{user:id}', [UserController::class, 'update']);
});

Route::group([
    'middleware' => ['auth:sanctum'],
    'prefix' => '/settings'
], function () {
    Route::get('/{setting:key}', [SettingController::class, 'show']);
});

Route::group([
    'prefix' => 'servers',
    'middleware' => ['auth:sanctum'],
], function () {
    Route::get('/', [ServerController::class, 'index']);
    Route::post('/', [ServerController::class, 'store']);
});

Route::group([
//    'middleware' => ['auth:sanctum'],
    'prefix' => '/nitrado'
], function () {
    Route::group([
        'prefix' => '/services'
    ], function () {
        Route::get('/', [ServiceController::class, 'index']);
        Route::get('/{id}', [ServiceController::class, 'show']);
    });
});

Route::get('/permissions', 'Permission\PermissionController')->middleware('auth:sanctum');

Route::any('/{any}', function () {
    abort(404);
})->where('any', '.*');