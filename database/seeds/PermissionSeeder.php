<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use App\Support\Permissions\Users;
use App\Support\Permissions\Servers;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => Users::LIST_USERS]);
        Permission::create(['name' => Users::MODIFY_USER]);
        Permission::create(['name' => Users::DELETE_USER]);
        Permission::create(['name' => Users::CREATE_USER]);
        Permission::create(['name' => Servers::ADD_SERVERS]);
        Permission::create(['name' => Servers::MODIFY_SERVERS]);
        Permission::create(['name' => Servers::DELETE_SERVERS]);
        Permission::create(['name' => Servers::LIST_SERVERS]);
    }
}
