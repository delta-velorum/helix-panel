<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('api_key', 255);
            $table->string('status_change_webhook')->nullable();

            $table->boolean('is_tracking_players')->default(true);
            $table->boolean('has_status_notifications')->default(false);

            $table->json('data');

            $table->timestamps();

            $table->index('has_status_notifications');
            $table->index('is_tracking_players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
